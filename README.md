This is a small test Python script to try a proof-of-concept with libraries [PyPDF2](https://pythonhosted.org/PyPDF2/) and [reportlab](https://pypi.python.org/pypi/reportlab) to automate transformations on PDF files.

I authorize Juan Camilo Peña and Sebastian Voigt to use and reproduce this script.

Author: Esteban Zacharzewski
