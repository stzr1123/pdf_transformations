from reportlab.pdfgen import canvas
import PyPDF2 as pypdf

f_name = 'sample.pdf'

c = canvas.Canvas('watermark.pdf')
c.drawImage('ad.jpg', 0, 0, width=600)
c.save()
ad = pypdf.PdfFileReader('watermark.pdf')
pdf = pypdf.PdfFileReader(f_name)

page = pdf.getPage(0)
new_page = page.createBlankPage(pdf)

# new_page.mergeScaledPage(page, 0.5)
new_page.mergeScaledTranslatedPage(page, 0.9, 0., 100.)
new_page.mergePage(ad.getPage(0))
new_pdf = pypdf.PdfFileWriter()
new_pdf.addPage(new_page)

f = open('sample_transformed.pdf', 'wb')
new_pdf.write(f)
f.close()
